import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ContactoService } from 'src/app/services/contacto.service';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.sass']
})
export class ContactoComponent implements OnInit {
  forma: FormGroup;

  constructor(private formBuilder:FormBuilder,private _contactS:ContactoService) {
    
   }

  ngOnInit(): void {
    this.forma = this.formBuilder.group({
      nombreC:new FormControl([""], [ Validators.required,Validators.minLength(8) ] ),
      nombreE:new FormControl([""], [ Validators.required] ),
      correo:new FormControl([""], [ Validators.required,Validators.email ] ),
      telefono:new FormControl([""], [ Validators.required ] ),
      categoria:new FormControl([""], [ Validators.required ] ),
      mensaje:new FormControl([""], [ Validators.required ] ),
    })
  }

  enviar(){
    //logic to send the form inside
    this._contactS.sendContactForm("payload").subscribe(
      (exitoso)=>{
        //formulado enviado
      },
      (err)=>{
        //ocurrio un error
      },
      ()=>{
        //request completado
      }
    )
  }

}
