import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContactoService {

  URL_SERVICIOS = "http://apiDeBackend.dominio";

  constructor(private http: HttpClient) { }

  // ======= SERVICIOS POST ====== //

  private postQuery<T>(query: string, data?: any){
    query = this.URL_SERVICIOS + query;
    return this.http.post<T>( query, data );
  }

  sendContactForm(payload:any){
    payload = JSON.stringify(payload)
    return this.postQuery(payload);
  }

  // ==================================//
  // ======= END SERVICIOS POST ====== //
  // ==================================//
}
